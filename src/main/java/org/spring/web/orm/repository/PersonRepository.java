package org.spring.web.orm.repository;

import org.spring.web.orm.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonRepository extends JpaRepository<Person, Integer> {
    
}
